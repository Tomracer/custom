-- prismarine
minetest.register_node("custom:prismarine", {
	description = "prismarine",
    tiles = {
        "prismarine.png",
    },
	groups = {cracky = 3, stone = 1},
    drop = 'custom:prismarine',
    legacy_mineral = false,
})

minetest.register_node("custom:red_prismarine", {
	description = "red prismarine",
    tiles = {
        "red_prismarine.png",
    },
	groups = {cracky = 3, stone = 1},
    drop = 'custom:red_prismarine',
    legacy_mineral = false,
})

minetest.register_node("custom:pink_prismarine", {
	description = "pink prismarine",
    tiles = {
        "pink_prismarine.png",
    },
    groups = {cracky = 3, stone = 1},
    drop = 'custom:pink_prismarine',
    legacy_mineral = false,
})

-- andesite

minetest.register_node("custom:cracked_andesite", {
	description = "cracked andesite",
    tiles = {
        "cracked_andesite.png",
    },
    groups = {cracky = 3, stone = 1},
    drop = 'custom:cracked_andesite',
    legacy_mineral = false,
})


minetest.register_node("custom:smooth_stone", {
	description = "smooth stone",
    tiles = {
        "smooth_stone.png",
    },
    groups = {cracky = 3, stone = 1},
    drop = 'custom:smooth_stone',
    legacy_mineral = false,
})
--hay_block
minetest.register_node("custom:hay_block", {
	description = "hay block",
    tiles = {
        "hay_block_top.png",
        "hay_block_top.png",
        "hay_block_side.png",
        "hay_block_side.png",
    },
    groups = {cracky = 3, stone = 1},
    drop = 'custom:hay block',
    legacy_mineral = false,
})

-- concrete wall blocks

minetest.register_node("custom:concrete_brick", {
	description = "concrete brick",
    tiles = {
        "concrete_brick.png",
    },
    groups = {cracky = 3, stone = 1},
    drop = 'custom:concrete_brick',
    legacy_mineral = false,
})

minetest.register_node("custom:concrete_brick_2", {
	description = "concrete brick 2",
    tiles = {
        "concrete_brick_2.png",
    },
    groups = {cracky = 3, stone = 1},
    drop = 'custom:concrete_brick_2',
    legacy_mineral = false,
})
