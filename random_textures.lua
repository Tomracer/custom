--shelf
minetest.register_node("custom:shelf", {
    description = "cobblestone",
    tiles = {
        "shelf1.png",
        "shelf_top.png",
        "shelf_side.png",
        "shelf_side.png",
    },
    groups = {cracky = 3, stone = 1, not_in_creative_inventory=1},
    drop = 'custom:cobblestone',
    legacy_mineral = false,

    on_place = function(itemstack, placer, pointed_thing)
    -- place a random bookshelf node
    local stack = ItemStack("custom:shelf" .. math.random(1, 6))
    local ret = minetest.item_place(stack, placer, pointed_thing)
    return ItemStack("custom:shelf " ..
        itemstack:get_count() - (1 - ret:get_count()))
end,
})

for i = 1, 6 do
minetest.register_node("custom:shelf" .. i, {
    description = "pink hardendclay",
    tiles = {
        "shelf_top.png",  
        "shelf_side.png",
        "shelf_side.png",
        "shelf_side.png",
        "shelf_top.png",
        "shelf" .. i .. ".png",
    },
    drop = "custom:shelf",
    groups = {snappy=1,choppy=2,oddly_breakable_by_hand=2,flammable=3, not_in_creative_inventory=1},
})
end


--coblestone
minetest.register_node("custom:cobblestone", {
    description = "cobblestone",
    tiles = {
        "cobblestone.png",
    },
    groups = {cracky = 3, stone = 1, not_in_creative_inventory=1},
    drop = 'custom:cobblestone',
    legacy_mineral = false,

    on_place = function(itemstack, placer, pointed_thing)
    -- place a random bookshelf node
    local stack = ItemStack("custom:cobblestone_alt_" .. math.random(1, 10))
    local ret = minetest.item_place(stack, placer, pointed_thing)
    return ItemStack("custom:cobblestone " ..
        itemstack:get_count() - (1 - ret:get_count()))
end,
})

for i = 1, 10 do
minetest.register_node("custom:cobblestone_alt_" .. i, {
    description = "pink hardendclay",
    tiles = {
        "cobblestone_alt_" .. i .. ".png",  
        "cobblestone_alt_" .. i .. ".png",
        "cobblestone_alt_" .. i .. ".png",
        "cobblestone_alt_" .. i .. ".png",
    },
    drop = "custom:cobblestone",
    groups = {snappy=1,choppy=2,oddly_breakable_by_hand=2,flammable=3, not_in_creative_inventory=1},
})
end



--hardened clay
    minetest.register_node("custom:hardenedclay_pink", {
	    description = "pink hardenedclay",
        tiles = {
            "hardened_clay_stained_pink_alt_1.png",
        },
	    groups = {cracky = 3, stone = 1, not_in_creative_inventory=1},
        drop = 'custom:hardenedclay_pink',
        legacy_mineral = false,

	    on_place = function(itemstack, placer, pointed_thing)
		-- place a random bookshelf node
		local stack = ItemStack("custom:hardend_clay_stained_pink_alt_" .. math.random(1, 2))
		local ret = minetest.item_place(stack, placer, pointed_thing)
		return ItemStack("custom:hardenedclay_pink " ..
			itemstack:get_count() - (1 - ret:get_count()))
	end,
})

for i = 1, 2 do
	minetest.register_node("custom:hardend_clay_stained_pink_alt_" .. i, {
		description = "pink hardendclay",
        tiles = {
            "hardened_clay_stained_pink_alt_" .. i .. ".png",    -- y+
            "hardened_clay_stained_pink_alt_" .. i .. ".png",  -- y-
            "hardened_clay_stained_pink_alt_" .. i .. ".png", -- x+
            "hardened_clay_stained_pink_alt_" .. i .. ".png",  -- x-
            "hardened_clay_stained_pink_alt_" .. i .. ".png",  -- z+
            "hardened_clay_stained_pink_alt_" .. i .. ".png", -- z-
        },
        --tiles = {"bookshelf_alt_" .. i .. ".png"},
		drop = "custom:bookshelf",
        groups = {snappy=1,choppy=2,oddly_breakable_by_hand=2,flammable=3, not_in_creative_inventory=1},
})
end
--bookshelf

minetest.register_node("custom:bookshelf", {
	description = "Bookshelf",
    tiles = {
        "curvedbookshelf_top.png",    -- y+
        "curvedbookshelf_top.png",  -- y-
        "curvedbookshelf_side.png", -- x+
        "curvedbookshelf_side.png",  -- x-
        "curvedbookshelf_side.png",  -- z+
        "bookshelf_alt_1.png", -- z-
    },
	groups = {cracky = 3, stone = 1, not_in_creative_inventory=1},
    drop = 'custom:bookshelf',
    legacy_mineral = false,

	on_place = function(itemstack, placer, pointed_thing)
		-- place a random bookshelf node
		local stack = ItemStack("custom:bookshelf_alt_" .. math.random(1, 12))
		local ret = minetest.item_place(stack, placer, pointed_thing)
		return ItemStack("custom:bookshelf " ..
			itemstack:get_count() - (1 - ret:get_count()))
	end,
})

for i = 1, 12 do
	minetest.register_node("custom:bookshelf_alt_" .. i, {
		description = "bookshelf",
        tiles = {
            "curvedbookshelf_top.png",    -- y+
            "curvedbookshelf_top.png",  -- y-
            "curvedbookshelf_side.png", -- x+
            "curvedbookshelf_side.png",  -- x-
            "curvedbookshelf_side.png",  -- z+
            "bookshelf_alt_" .. i .. ".png", -- z-
        },
        --tiles = {"bookshelf_alt_" .. i .. ".png"},
		drop = "custom:bookshelf",
        groups = {snappy=1,choppy=2,oddly_breakable_by_hand=2,flammable=3, not_in_creative_inventory=1},
})
end
--more bookshelfs lol
minetest.register_node("custom:bookshelf_curved", {
	description = "curved bookshelf",
    tiles = {
        "curvedbookshelf_top.png",    -- y+
        "curvedbookshelf_top.png",  -- y-
        "curvedbookshelf_side.png", -- x+
        "curvedbookshelf_side.png",  -- x-
        "curvedbookshelf_side.png",  -- z+
        "top_shelf_1.png", -- z-
    },
	groups = {cracky = 3, stone = 1, not_in_creative_inventory=1},
	drop = 'custom:bookshelf_curved',
	legacy_mineral = false,

	on_place = function(itemstack, placer, pointed_thing)
		-- place a random curved bookshelf node
		local stack = ItemStack("custom:top_shelf_" .. math.random(1, 14))
		local ret = minetest.item_place(stack, placer, pointed_thing)
		return ItemStack("custom:bookshelf_curved " ..
			itemstack:get_count() - (1 - ret:get_count()))
	end,
})

for t = 1, 14 do
	minetest.register_node("custom:top_shelf_" .. t, {
		description = "curved bookshelf",
        tiles = {
            "curvedbookshelf_top.png",    -- y+
            "curvedbookshelf_top.png",  -- y-
            "curvedbookshelf_side.png", -- x+
            "curvedbookshelf_side.png",  -- x-
            "curvedbookshelf_side.png",  -- z+
            "top_shelf_" .. t .. ".png", -- z-
        },
        --tiles = {"bookshelf_alt_" .. i .. ".png"},
		drop = "custom:bookshelf_curved",
		groups = {cracky = 3, stone = 1, not_in_creative_inventory=1},
})
end
-- bricks
minetest.register_node("custom:brick", {
	description = "brick",
    tiles = {
        "brick_alt_1.png",
    },
	groups = {cracky = 3, stone = 1, not_in_creative_inventory=1},
    drop = 'custom:brick',
    legacy_mineral = false,

	on_place = function(itemstack, placer, pointed_thing)
		-- place a random bookshelf node
		local stack = ItemStack("custom:bookshelf_alt_" .. math.random(1, 7))
		local ret = minetest.item_place(stack, placer, pointed_thing)
		return ItemStack("custom:bookshelf " ..
			itemstack:get_count() - (1 - ret:get_count()))
	end,
})

for i = 1, 7 do
	minetest.register_node("custom:bookshelf_alt_" .. i, {
		description = "brick",
        tiles = {
            "brick_alt_" .. i .. ".png",    -- y+
            "brick_alt_" .. i .. ".png",  -- y-
            "brick_alt_" .. i .. ".png", -- x+
            "brick_alt_" .. i .. ".png",  -- x-
            "brick_alt_" .. i .. ".png",  -- z+
            "brick_alt_" .. i .. ".png", -- z-
        },
        --tiles = {"bookshelf_alt_" .. i .. ".png"},
		drop = "custom:bookshelf",
        groups = {snappy=1,choppy=2,oddly_breakable_by_hand=2,flammable=3, not_in_creative_inventory=1},
})
end
--sandstone bricks



minetest.register_node("custom:sandstone_brick", {
	description = "sandstonebrick",
    tiles = {
        "sandstone_smooth_alt1.png",    -- y+
        "sandstone_smooth_alt1.png",  -- y-
        "sandstone_smooth_alt1.png", -- x+
        "sandstone_smooth_alt1.png",  -- x-
        "sandstone_smooth_alt1.png",  -- z+
        "sandstone_smooth_alt1.png", -- z-
    },
	groups = {cracky = 3, stone = 1, not_in_creative_inventory=1},
	drop = 'custom:sandstone_brick',
	legacy_mineral = false,

	on_place = function(itemstack, placer, pointed_thing)
		-- place a random sandstone_brick node
		local stack = ItemStack("custom:sandstone_smooth_alt" .. math.random(1,17))
		local ret = minetest.item_place(stack, placer, pointed_thing)
		return ItemStack("custom:sandstone_brick " ..
			itemstack:get_count() - (1 - ret:get_count()))
	end,
})

for i = 1, 17 do
	minetest.register_node("custom:sandstone_smooth_alt" .. i, {
		description = "sandstonebrick",
        tiles = {
            "sandstone_smooth_alt" .. i .. ".png",    -- y+
            "sandstone_smooth_alt" .. i .. ".png",  -- y-
            "sandstone_smooth_alt" .. i .. ".png", -- x+
            "sandstone_smooth_alt" .. i .. ".png",  -- x-
            "sandstone_smooth_alt" .. i .. ".png",  -- z+
            "sandstone_smooth_alt" .. i .. ".png", -- z-
        },
		drop = "custom:sandstone_brick",
		groups = {cracky = 3, stone = 1, not_in_creative_inventory=1},
})
end


--sandstone_corner's


minetest.register_node("custom:sandstone_corner", {
	description = "sandstone corner",
    tiles = {
        "sandstone_corner_1.png",    -- y+
        "sandstone_corner_1.png",  -- y-
        "sandstone_corner_1.png", -- x+
        "sandstone_corner_1.png",  -- x-
        "sandstone_corner_1.png",  -- z+
        "sandstone_corner_1.png", -- z-
    },
	groups = {cracky = 3, stone = 1, not_in_creative_inventory=1},
	drop = 'custom:sandstone_corner',
	legacy_mineral = false,

	on_place = function(itemstack, placer, pointed_thing)
		-- place a random sandstone_brick node
		local stack = ItemStack("custom:sandstone_corner_" .. math.random(1, 6))
		local ret = minetest.item_place(stack, placer, pointed_thing)
		return ItemStack("custom:sandstone_corner " ..
			itemstack:get_count() - (1 - ret:get_count()))
	end,
})

for i = 1, 6 do
	minetest.register_node("custom:sandstone_corner_" .. i, {
		description = "sandstonebrick",
        tiles = {
            "sandstone_corner_" .. i .. ".png",    -- y+
            "sandstone_corner_" .. i .. ".png",  -- y-
            "sandstone_corner_" .. i .. ".png", -- x+
            "sandstone_corner_" .. i .. ".png",  -- x-
            "sandstone_corner_" .. i .. ".png",  -- z+
            "sandstone_corner_" .. i .. ".png", -- z-
        },
		drop = "custom:sandstone_corner",
		groups = {cracky = 3, stone = 1, not_in_creative_inventory=1},
})

--glass panes

minetest.register_node("custom:window_1", {
    description = "window 1",
    tiles = {
		"cyan_stained_glass_pane_top_2.png",
		"cyan_stained_glass_pane_top_2.png",
		"cyan_stained_glass_pane_top.png",
		"cyan_stained_glass_pane_top.png",
		"cyan_stained_glass.png",
		"cyan_stained_glass.png"
	},
	drawtype = "nodebox",
    paramtype = "light",
    groups = {cracky=3,oddly_breakable_by_hand=3},
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.125, 0.5, 0.5, 0.0625}, -- NodeBox1
		}
	}
})

minetest.register_node("custom:window", {
    description = "window",
    tiles = {
		"cyan_stained_glass_pane_top_2.png",
		"cyan_stained_glass_pane_top_2.png",
		"cyan_stained_glass_pane_top.png",
		"cyan_stained_glass_pane_top.png",
		"window.png",
		"window.png"
	},
	drawtype = "nodebox",
    paramtype = "light",
    groups = {cracky=3,oddly_breakable_by_hand=3},
	node_box = {
		type = "fixed",
		fixed = {
			{-0.5, -0.5, -0.125, 0.5, 0.5, 0.0625}, -- NodeBox1
		}
	}
})
end